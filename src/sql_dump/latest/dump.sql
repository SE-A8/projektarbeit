-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 26. Okt 2017 um 12:03
-- Server Version: 5.5.57-0+deb8u1
-- PHP-Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `swe_project`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_benutzer`
--

CREATE TABLE IF NOT EXISTS `tbl_benutzer` (
  `nachname` text NOT NULL,
  `vorname` text NOT NULL,
  `email` text NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titelbild_url` blob NOT NULL,
  `profilbild_url` blob NOT NULL,
  `geburtstag` date NOT NULL,
  `passwort` text NOT NULL COMMENT 'sh265 hash',
  `session_id` int(11) NOT NULL,
  `nutzerbeschreibung` text NOT NULL,
  `firmennamen` text NOT NULL,
  `telefonnummer` text NOT NULL,
  `ist_firmenaccount` tinyint(1) NOT NULL,
  `pk_id_user` int(11) NOT NULL,
  `strasse` text NOT NULL,
  `hausnummer` text NOT NULL,
  `fk_bewertung_id` int(11) NOT NULL,
  `fk_social_id` int(11) NOT NULL,
  `fk_plz_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_bestellung`
--

CREATE TABLE IF NOT EXISTS `tbl_bestellung` (
`pk_bestellung_id` int(11) NOT NULL,
  `mietanfang` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mietende` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fk_benutzer_mieter` int(11) NOT NULL,
  `fk_benutzer_vermieter` int(11) NOT NULL,
  `fk_inserat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_bewertung`
--

CREATE TABLE IF NOT EXISTS `tbl_bewertung` (
`pk_bewertung_id` int(11) NOT NULL,
  `titel_text` text NOT NULL,
  `bewertung_text` text NOT NULL,
  `bewertung_prozent` int(11) NOT NULL COMMENT '0-100%'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_inserat`
--

CREATE TABLE IF NOT EXISTS `tbl_inserat` (
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` text NOT NULL,
  `desc` text NOT NULL,
  `images` blob NOT NULL,
  `fahrradtyp` int(11) NOT NULL,
  `fk_typ` int(11) NOT NULL,
`pk_inserat_id` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `preis` float NOT NULL,
  `ist_verfuegbar` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_plz`
--

CREATE TABLE IF NOT EXISTS `tbl_plz` (
  `pk_plz_id` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_social`
--

CREATE TABLE IF NOT EXISTS `tbl_social` (
`pk_social_id` int(11) NOT NULL,
  `facebook_url` text NOT NULL,
  `instagram_url` text NOT NULL,
  `generic_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_typ`
--

CREATE TABLE IF NOT EXISTS `tbl_typ` (
`pk_typ_id` int(11) NOT NULL,
  `typ` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_benutzer`
--
ALTER TABLE `tbl_benutzer`
 ADD PRIMARY KEY (`pk_id_user`);

--
-- Indizes für die Tabelle `tbl_bestellung`
--
ALTER TABLE `tbl_bestellung`
 ADD PRIMARY KEY (`pk_bestellung_id`);

--
-- Indizes für die Tabelle `tbl_bewertung`
--
ALTER TABLE `tbl_bewertung`
 ADD PRIMARY KEY (`pk_bewertung_id`);

--
-- Indizes für die Tabelle `tbl_inserat`
--
ALTER TABLE `tbl_inserat`
 ADD PRIMARY KEY (`pk_inserat_id`);

--
-- Indizes für die Tabelle `tbl_plz`
--
ALTER TABLE `tbl_plz`
 ADD PRIMARY KEY (`pk_plz_id`);

--
-- Indizes für die Tabelle `tbl_social`
--
ALTER TABLE `tbl_social`
 ADD PRIMARY KEY (`pk_social_id`);

--
-- Indizes für die Tabelle `tbl_typ`
--
ALTER TABLE `tbl_typ`
 ADD PRIMARY KEY (`pk_typ_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_bestellung`
--
ALTER TABLE `tbl_bestellung`
MODIFY `pk_bestellung_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `tbl_bewertung`
--
ALTER TABLE `tbl_bewertung`
MODIFY `pk_bewertung_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `tbl_inserat`
--
ALTER TABLE `tbl_inserat`
MODIFY `pk_inserat_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `tbl_social`
--
ALTER TABLE `tbl_social`
MODIFY `pk_social_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `tbl_typ`
--
ALTER TABLE `tbl_typ`
MODIFY `pk_typ_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
